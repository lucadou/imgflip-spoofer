// ==UserScript==
// @name         Imgflip AI Meme Spoofer
// @namespace    http://tampermonkey.net/
// @version      1.0.2
// @description  Allows you to spoof AI generated memes on Imgflip.
// @author       Luna Lucadou
// @homepage     https://luna.lucadou.sh/
// @source       https://gitlab.com/lucadou/imgflip-spoofer
// @updateURL    https://gitlab.com/lucadou/imgflip-spoofer/-/raw/master/src/spoofer.js
// @downloadURL  https://gitlab.com/lucadou/imgflip-spoofer/-/raw/master/src/spoofer.js
// @match        https://imgflip.com/ai-meme
// @match        https://imgflip.com/ai-meme/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    let meme_canvas = document.querySelector('.mm-canv'); // Canvas object
    let canvas_dim_template = 'Canvas info: width: {w}, height: {h}';
    let context = meme_canvas.getContext('2d');
    let fake_image = new Image();
    fake_image.onload = function() {
        context.drawImage(fake_image, 0, 0);
        console.log('Painted image on the canvas');
    }; // https://stackoverflow.com/a/4409745

    let get_canvas_dimensions = function() {
      return [meme_canvas.getAttribute('width'),
              meme_canvas.getAttribute('height')];
    };

    // Create file uploader
    let create_file_upload = function() {
      let img_form = document.createElement('form');
      img_form.setAttribute('id', 'file-overwriter-form');
      let input_help1 = document.createElement('div');
      input_help1.innerHTML = 'Add an image:';
      let input_file = document.createElement('input');
      input_file.setAttribute('type', 'file');
      input_file.setAttribute('id', 'file-overwriter-upload');
      input_file.setAttribute('accept', 'image/png, image/jpeg');
      let input_help2 = document.createElement('div');
      input_help2.innerHTML = 'Files will automatically add when selected. Does not resize before drawing.';
      let input_help3 = document.createElement('div');
      input_help3.setAttribute('id', 'input-dim-text');
      input_help3.innerHTML = canvas_dim_template.split('{w}')
                                .join(get_canvas_dimensions()[0])
                                .split('{h}')
                                .join(get_canvas_dimensions()[1]);
      let input_error = document.createElement('span');
      input_error.setAttribute('style', 'display: none;');
      img_form.appendChild(input_help1);
      img_form.appendChild(input_file);
      img_form.appendChild(input_help2);
      img_form.appendChild(input_help3);
      img_form.appendChild(input_error);

      input_file.addEventListener('change', function(e) {
        let files = e.target.files; // Returns a FileList object
        if (files.length === 1) {
            input_error.setAttribute('style', 'display: none;');
            // Get file
            let file = files[0];
            let reader = new FileReader();
            // Closure to capture the file information
            reader.onload = (function(theFile) {
                return function(e) {
                    fake_image.src = e.target.result;
                };
            })(file);

            // Read in the image file as a data URL.
            reader.readAsDataURL(file);
        }
      }, false); // Based on https://stackoverflow.com/a/39515846

      let left_element = document.getElementById('aim-text');
      left_element.insertAdjacentElement('afterend', img_form);
    };
    create_file_upload();

    // Setup observer for canvas changes
    let observer = new MutationObserver(function(mutations, observer) {
        /* Fired when a mutation occurs (within 'div.mm-preview.shadow.no-events', aka when the user changes the meme template).
         * Does not fire when the canvas has a new image drawn over it.
         * https://stackoverflow.com/a/39332340
         */
        let info = document.getElementById('input-dim-text');
        info.innerHTML = canvas_dim_template.split('{w}')
                          .join(get_canvas_dimensions()[0])
                          .split('{h}')
                          .join(get_canvas_dimensions()[1]);
    }).observe(document.querySelector('div.mm-preview.shadow.no-events'), {childList: true});
    // chidList - Set to true if mutations to target’s children are to be observed.
    // https://dom.spec.whatwg.org/#interface-mutationobserver
})();