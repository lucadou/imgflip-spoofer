# Changelog
All notable changes to this project will be documented in this file.

## [1.0.2] - 2020-05-05

### Changed

- Moved some comments around
- Removed some unecessary comments

## [1.0.1] - 2020-05-05

### Changed

- Fixed the URLs the script matched

## [1.0.0] - 2020-05-05

### Added

- License added
- Repo created
- Script created
