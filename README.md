# Imgflip AI Meme Spoofer

You have probably seen some of Imgflip's AI-generated memes (such as [this](https://i.imgflip.com/404vsv.jpg), which is an actual AI-generated Doge meme). But did you know that Imgflip just downloads the image to your browser and then relies on your browser reuploading the image before it gets watermarked as an AI-generated meme? Whatever you write to the HTML5 Canvas that renders the meme will be re-uploaded. So all it takes is a small script to insert your own images and spoof AI-generated memes.

## Getting Started

### Prerequisites

* Any modern browser.
* Tampermonkey or another userscript extension.

### Installation

* Open Tampermonkey Dashboard.
* Go to the Utilities tab.
* Inside the "Install from URL" box, paste the link
`https://gitlab.com/lucadou/imgflip-spoofer/-/raw/master/src/spoofer.js`
and click "Install".
* Click the "Install" or "Update" button that appears on the next page.

## FAQ

> Why was this created?

Because the user OLDMARIO posted [this](https://i.imgflip.com/404dpz.jpg) in MonotoneTim's Twitch chat. That gave me an idea: what if I put the AI meme caption on it? But I had a problem: uploading to Imgur would be a dead giveaway that it was a fake. So, I opened up Chrome developer tools on the AI meme page and watched the network requests when I hit the "Save Meme" button - it POSTs the contents of the canvas as bas64. After a bit of toying around in the console, I had figured out how to spoof AI-generated memes and uploaded [this](https://i.imgflip.com/404fh4.jpg).

> Can this be prevented?

Yes, it can be prevented quite easily. All Imgflip has to do is tighten up their system. They could store the text of the meme and the template name in a database, then just POST the ID to save of the AI-generated meme, and return a link with the meme. That would completely break the ability of this script to work. But given that this is the same method used by their main meme generator, I doubt they will be changing it anytime soon unless a ton of people start using this.

> Does this resize images to the canvas size before inserting?

No. If your image is larger than the canvas, the overflow will be hidden. I have tried adjusting the width and height of the canvas, but it breaks it completely. Because of this, I display the current width and height of the canvas so you can resize your images before uploading them.

Since this just layers on top in the canvas, it has the side effect of allowing you to insert images that do not cover the entire canvas. This can be used to create some [amusing images](https://i.imgflip.com/404qox.jpg) (slightly NSFW). You can add as many images as you like, they just cover up the previous ones.

## Versioning

We use Semantic Versioning, aka [SemVer](https://semver.org/spec/v2.0.0.html).

## Authors

* Luna Lucadou - Wrote the script

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE.md](LICENSE.md) file for details.
